# Devops Previous Lesson

## Clone pytime_git_practice repo

```
git clone https://github.com/dev-paplix/pytime_git_practice.git
```

## Copy .gitignore from pytime_git_practice to devops-previous-lesson

```
cp ./pytime_git_practice/.gitignore ./devops-previous-lesson
```

## Change directory to devops-previous-lesson

```
cd devops-previous-lesson
```

## Create MY_DEVOPS_GIT directory

```
mkdir MY_DEVOPS_GIT
```

## Move files and folders to MY_DEVOPS_GIT

```
mv yasirsoleh.py .gitignore .venv ./MY_DEVOPS_GIT
```

## Create new repository in GitLab

[Create New Project](https://gitlab.com/projects/new)

## Init a new git repository

```
git init
```

## Stage the changes

```
git add .
```

## Create commit

```
git commit -m "initial commit"
```

## Add origin (one time only)

```
git remote add origin git@gitlab.com:yasirsoleh/devops-previous-lesson.git
```

## Push (--set-upstream option is only one time)

```
git push --set-upstream origin master
```

# Building and Running Docker Container

## Write Dockerfile

```
FROM base-image
WORKDIR /app
COPY . /app
RUN commands
EXPOSE port-number
CMD ["app-binary", "args"]
```

Example:

```
FROM python:3.10
WORKDIR /app
COPY ./MY_DEVOPS_GIT .
RUN python -m pip install --upgrade pip
RUN python3 -m venv .venv
RUN . .venv/bin/activate && python3 -m pip install DateTime
CMD . .venv/bin/activate && python3 yasirsoleh.py
```

## Build Docker Image

```
docker build -t image-name .
```

Example:

```
docker build -t devops-previous-lesson .
```

## Listing Image

```
docker images ls
```

## Run the Container from Image

```
docker run --name container-name image-name
```

Example:

```
docker run --name devops-previous-lesson devops-previous-lesson:latest
```

## List all container (including exited)

```
docker container ls -a
```

## Delete Container

```
docker container rm container-name
```

Example:

```
docker container rm devops-previous-lesson
```

## Delete Image

```
docker image rm image-name
```

Example

```
docker image rm devops-previous-lesson:latest
```
