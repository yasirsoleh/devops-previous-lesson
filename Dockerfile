FROM python:3.10

WORKDIR /app

COPY ./MY_DEVOPS_GIT .

RUN python -m pip install --upgrade pip

RUN python3 -m venv .venv

RUN . .venv/bin/activate && python3 -m pip install DateTime

CMD . .venv/bin/activate && python3 yasirsoleh.py